# %TASK_NAME%
**Сложность:** Лёгкая/Средняя/Тяжёлая, **Тэги: ** admin, crypto, etc...
### Описание
%будет видно всем участникам на сайте%
### Уязвимость
%будет видна только администраторам, для тестирования%
### Эксплуатация
%будет видна участникам после окончания турниров. write-up%
### Подсказки
1. нумерованный список с подсказками
2. их будут видеть участники по ходу турнира, если задание будет нерешаемым


example main.json

	{
		"game":"Free Hack Quest 2016",
		"name":"task name",
		"category":"admin",
		"status": "need verify",
		"description":{
			"RU":"",
			"EN":""
		},
		"hints":[
			"RU":"",
			"EN":""
		],
		"value":0,
		"flag_type":"static",
		"flag_key":"",
		"files":[
			{
				"name":"",
				"location":""
			}
		],
		"links":[
			{
				"dropbox":"",
				"yandex":""
			}
		],
		"authors":[
			{
				"name":"",
				"team":"",
				"contacts":[
					""
				]
			}
		]
	}
