# Free Hack Quest 2016

## Short list of tasks

 * admin 100  "repo" by [keva] sea-kg (mrseakg@gmail.com)
 * stego 100  "alalax" by [keva] sea-kg (mrseakg@gmail.com), [keva] f0x (fox.user.3@gmail.com)
 * unknown 0  "./stego/designer" by ()
 * stego 400  "greed" by [keva] sea-kg (mrseakg@gmail.com)


## Errors

 * ./admin/repo
	 * main.json: Wrong value of field "flag_key" must be format "FHQ(`md5`) or FHQ(`sometext`)"

## Statistics by categories

|Category|Count|Summary value
|---|---|---
|admin|1|100
|stego|2|500
|unknown|1|0
|All|4|600


## Status table

|Category&Value|Name|Status|Author(s)
|---|---|---|---
|admin 100|repo|need verify|[keva] sea-kg (mrseakg@gmail.com)
|stego 100|alalax|need verify|[keva] sea-kg (mrseakg@gmail.com), [keva] f0x (fox.user.3@gmail.com)
|stego 400|greed|need verify|[keva] sea-kg (mrseakg@gmail.com)
|unknown 0|./stego/designer|invalid json|()
