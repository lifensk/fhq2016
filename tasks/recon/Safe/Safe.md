##Safe##
-------------
###Описание###
В углу комнаты был найден сейф и рядом на стене странный набор цифр:  
     |97814|
     |05851|
     |36711|
     |2118 |
     
вам нужно то,что лежит в сейфе.

###Решение###
Используя информацию из хинтов о том,что это международный стандарт каталогов методом глубокого гугления понимаем,что это международный стандартный книжный номер
ISBN,по нему находим книгу- англо-английский словарь Longman ,оставшиеся цифры указывают на страницу и номер слова на данной странице.


### Хинты###
1.Международный стандарт
2.Каталог
3.Лишние цифры-не лишние