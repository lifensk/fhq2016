#!/usr/bin/python
# -*- coding: utf-8 -*-

infile = open("gact.png", "rb")
outfile = open("../gact.txt", "w")

def convertToC(c):
	if c == 0:
		return "G";
	if c == 1:
		return "A";
	if c == 2:
		return "C";
	if c == 3:
		return "T";

def writeByte(byte, n):
	b = ord(byte);
	g = 0
	st = []
	while g < 4:
		c = b % 4;
		b = (b - c) / 4;
		st.append(convertToC(c))
		g = g + 1
	st.reverse()
	
	for c in st:
		n = n + 1
		outfile.write(c)
		if n % 54 == 0:
			outfile.write("\n")
		elif n % 3 == 0:
			outfile.write("-")

	# print(str(ord(byte)) + " = " + "".join(st));
	return n

# 78 => 01 00 11 10  => 1 0 3 2 AGTC
# GACT = 0123

try:
	n = 0
	byte = infile.read(1)
	n = writeByte(byte, n)
	while byte != "":
		byte = infile.read(1);
		if byte != "":
			n = writeByte(byte, n);
finally:
    infile.close()
    outfile.close();


