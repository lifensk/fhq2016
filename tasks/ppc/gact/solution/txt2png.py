#!/usr/bin/python
# -*- coding: utf-8 -*-

infile = open("../gact.txt", "r")
outfile = open("tmp_gact.png", "wb")

def convertToInt(c):
	if c == "G":
		return 0;
	if c == "A":
		return 1;
	if c == "C":
		return 2;
	if c == "T":
		return 3;

def writeByte(st):
	c0 = convertToInt(st[0])
	c1 = convertToInt(st[1])
	c2 = convertToInt(st[2])
	c3 = convertToInt(st[3])
	c = c0*(4*4*4) + c1*(4*4) + c2*4 + c3
	# print(st + " => " + str(c) ) # TODO WRITE
	outfile.write(chr(c))

def collect(byte, st):
	if byte == "G" or byte == "A" or byte == "C" or byte == "T":
		st = st + str(byte)
	# print(byte)
	if len(st) == 4:
		writeByte(st)
		st = ""
	return st

try:
	n = 0
	st = ""
	byte = infile.read(1)
	st = collect(byte, st)
	while byte != "":
		byte = infile.read(1);
		if byte != "":
			st = collect(byte, st)
finally:
    infile.close()
    outfile.close();
