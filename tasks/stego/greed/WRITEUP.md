# Greed

## Solution by author

* download program: `git clone https://github.com/sea-kg/stego-packer.git stego-packer.git`
* make this program `qmake & make` (qt-sdk must be installed)
* call `./packer unpack greed.png`


## Generate

`./packer pack orig/greed.png greed.png "FHQ(lsbisaclassicforsteganography)"`

check:

`./packer unpack greed.png`
