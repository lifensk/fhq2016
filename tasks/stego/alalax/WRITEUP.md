
# Alalax

## Solution by authors

* Open with gimp
* Take 3'rd layout
* Detect MaxiCode_2D
* Read decode online https://zxing.org/w/decode.jspx

## Links

* Generate MaxiCode:
	http://barcode.tec-it.com/ru#

* Decode:
	https://zxing.org/w/decode.jspx
